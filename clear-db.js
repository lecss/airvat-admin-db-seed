const admin = require('./firebase-app').app;

function deleteCollection(db, collectionPath, batchSize) {
  var collectionRef = db.collection(collectionPath);
  var query = collectionRef.orderBy('__name__').limit(batchSize);

  return new Promise((resolve, reject) => {
    return deleteQueryBatch(db, query, batchSize, resolve, reject);
  });
}

function deleteQueryBatch(db, query, batchSize, resolve, reject) {
  return query.get()
    .then((snapshot) => {
      // When there are no documents left, we are done
      if (snapshot.size == 0) {
        return 0;
      }

      // Delete documents in a batch
      var batch = db.batch();
      snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref);
      });

      return batch.commit().then(() => {
        return snapshot.size;
      });
    }).then((numDeleted) => {
      if (numDeleted === 0) {
        console.log('All documents removed from firestore');
        resolve();
        return;
      }
      console.log('Removed next portion with count:', numDeleted);

      // Recurse on the next process tick, to avoid
      // exploding the stack.
      process.nextTick(() => {
        deleteQueryBatch(db, query, batchSize, resolve, reject);
      });
    })
    .catch(reject);
}


async function deleteAllUsers() {

    return  new Promise((reslove, reject) => {
        admin.auth().listUsers(1000)
            .then(function(listUsersResult) {
                listUsersResult.users.forEach(async function(userRecord) {
                    return  admin.auth().deleteUser(userRecord.uid)
                        .then(function() {
                            console.log('Successfully deleted user');
                        })
                        .catch(function(error) {
                            console.log('Error deleting user:', error);
                        });

                });
            })
            .catch(() => reject());
    })
}


(async function clearDatabaseAndFirestore () {
  await admin.database().ref().set('/', null);
  console.log('Realtime database cleared');
  await deleteCollection(admin.firestore(), 'users', 20);
  await deleteAllUsers();
  process.exit(0);
})()
