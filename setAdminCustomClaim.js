const admin = require('./firebase-app').app;
const adminUid = require("./firebase-config").adminUid;

admin.auth().setCustomUserClaims(adminUid, {
    admin: true,
    canRoleManagement: true
}).then(() => {
    console.log("Done!!");
    process.exit(0);
});
